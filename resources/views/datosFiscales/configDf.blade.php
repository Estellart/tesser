@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        @if(session('message'))
        <div class='alert-success'>
        {{session('message')}}
        </div>
        @endif
            <div class="card">
                <div class="card-header">Configuración de mis datos fiscales</div>
                <!-- Está es la información dentro de la página Datos Fiscales en la cual podremos editar información-->
                <div class="card-body">
                    <form method="POST" action="{{ route('datosFiscales.update') }}">
                        @csrf           
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('DNI') }}</label>

                            <div class="col-md-6">
                                <input id="dni" type="varchar" class="form-control @error('dni') is-invalid @enderror" name="dni" value="{{ Auth::user()->dni }}" required autocomplete="dni" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>




                        <div class="form-group row">
                            <label for="domicilio" class="col-md-4 col-form-label text-md-right">{{ __('Domicilio') }}</label>

                            <div class="col-md-6">
                                <input id="domicilio" type="text" class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{ Auth::user()->domicilio }}" required autocomplete="domicilio" autofocus>

                                @error('surname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>







                        <div class="form-group row">
                            <label for="trabajo" class="col-md-4 col-form-label text-md-right">{{ __('Puesto de Trabajo') }}</label>

                            <div class="col-md-6">
                                <input id="trabajo" type="text" class="form-control @error('trabajo') is-invalid @enderror" name="trabajo" value="{{ Auth::user()->trabajo }}" required autocomplete="trabajo">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="salario" class="col-md-4 col-form-label text-md-right">{{ __('Salario') }}</label>

                            <div class="col-md-6">
                                <input id="salario" type="text" class="form-control @error('salario') is-invalid @enderror" name="salario" value="{{ Auth::user()->salario }}" required autocomplete="salario">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Guardar cambios
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection