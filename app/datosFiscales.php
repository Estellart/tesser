<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;



class datosFiscales extends Model
{
    
    protected $table = 'datosFiscales';

    use Notifiable;

    protected $fillable = [
        'dni','domicilio','trabajo', 'salario',
    ];
}
