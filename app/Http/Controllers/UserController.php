<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{   
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    //Devuelve la vista de la página/pestaña configuración
    public function config(){
        return view('user.config');
    }

    public function update(Request $request){
        //Conseguir el usuario identificado
        $user = \Auth::user();
        $id = $user->id;
        
        //Validación del formulario
        $validate = $this->validate($request,[
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' .$id],
        ]);
       
        //Recoger datos del formulario
       $name = $request->input('name');
       $surname = $request->input('surname');
       $email = $request->input('email');

        //Asignar nuevos valores al objeto del usuario
       $user->name = $name;
       $user->surname = $surname;
       $user->email = $email;

       //Ejecutar la consola y cambios en la base de datos
       $user->update();

       //Enviar un mensaje como verificación de que el usuario ha sido actualizado correctamente
       return redirect()->route('config')
                        ->with(['message'=>'Usuario actualizado correctamente!']);


      
    }
}
