<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class datosFiscalesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
       //Devuelve la vista de la página/pestaña configuración
        public function configDf(){
            return view('datosFiscales.configDf');
        }
    
        public function updateDf(Request $request){
            //Conseguir el usuario identificado
            $user = \Auth::user();
            $id = $user->user_id;
            
            //Validación del formulario
            $validate = $this->validate($request,[
                'user_id' => ['required', 'int', 'max:9'],
                'dni' => ['required', 'string', 'max:9'],
                'domicilio' => ['required', 'string', 'max:255'],
                'trabajo' => ['required', 'string','max:255'],
                'salario' => ['required', 'string','max:6'],
            ]);
           
            //Recoger datos del formulario
           $dni = $request->input('dni');
           $domicilio = $request->input('domicilio');
           $trabajo = $request->input('trabajo');
           $salario = $request->input('salario');
    
            //Asignar nuevos valores al objeto de datos fiscales
           $user->dni = $dni;
           $user->domicilio = $domicilio;
           $user->trabajo = $trabajo;
           $user->salario = $salario;
    
           //Ejecutar la consola y cambios en la base de datos
           $user->updateDf();
    
           //Enviar un mensaje como verificación de que los datos fiscales han sido actualizados correctamente
           return redirect()->route('configDf')
                            ->with(['message'=>'Datos actualizados correctamente!']);
    
                
          
        }
    }

